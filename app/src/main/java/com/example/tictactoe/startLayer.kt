package com.example.tictactoe

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class startLayer: AppCompatActivity(), View.OnClickListener {

    private lateinit var name1: String
    private lateinit var name2: String

    private lateinit var button1: Button
    private lateinit var button2: Button
    private lateinit var button3: Button
    private lateinit var button4: Button
    private lateinit var button5: Button
    private lateinit var button6: Button
    private lateinit var button7: Button
    private lateinit var button8: Button
    private lateinit var button9: Button

    private lateinit var restartButton: Button
    private lateinit var resetButton: Button

    private lateinit var turn: TextView

    private var scoreX = 0
    private var scoreO = 0

    private lateinit var player1: TextView
    private lateinit var player2: TextView

    private lateinit var arrowButton: Button

    private var activePlayer = 1
    private var firstPlayer = ArrayList<Int>()
    private var secondtPlayer = ArrayList<Int>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start_layer)


        turn = findViewById(R.id.turn)


        arrowButton = findViewById(R.id.arrowBack)
        arrowButton.setOnClickListener{

            val back = Intent (this, MainActivity::class.java)
            startActivity(back)
            finish()

        }

        init()
        name1 = intent?.extras?.getString("name1", "Player I").toString()
        name2 = intent?.extras?.getString("name2", "Player II").toString()

        if (name1.isEmpty()){
            name1 = "Player I"
        }
        if (name2.isEmpty()){
            name2 = "Player II"
        }

        turn.text = "turn $name1"




        resetButton = findViewById((R.id.resetButton))
        resetButton.setOnClickListener{reset()}

        player1 = findViewById(R.id.player1)
        player2 = findViewById(R.id.player2)
        player1.text = "$name1 - 0"
        player2.text = "0 - $name2"
    }

    private fun init() {
        button1 = findViewById(R.id.button1)
        button2 = findViewById(R.id.button2)
        button3 = findViewById(R.id.button3)
        button4 = findViewById(R.id.button4)
        button5 = findViewById(R.id.button5)
        button6 = findViewById(R.id.button6)
        button7 = findViewById(R.id.button7)
        button8 = findViewById(R.id.button8)
        button9 = findViewById(R.id.button9)

        button1.setOnClickListener(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)
        button4.setOnClickListener(this)
        button5.setOnClickListener(this)
        button6.setOnClickListener(this)
        button7.setOnClickListener(this)
        button8.setOnClickListener(this)
        button9.setOnClickListener(this)

        restartButton = findViewById(R.id.restartButton)
        restartButton.setOnClickListener{
            reset()
            scoreX = 0
            scoreO = 0
        }
    }

    override fun onClick(clickedView: View?) {
        if (clickedView is Button) {
            var buttonNumber = 0
            when (clickedView.id) {
                R.id.button1 -> buttonNumber = 1
                R.id.button2 -> buttonNumber = 2
                R.id.button3 -> buttonNumber = 3
                R.id.button4 -> buttonNumber = 4
                R.id.button5 -> buttonNumber = 5
                R.id.button6 -> buttonNumber = 6
                R.id.button7 -> buttonNumber = 7
                R.id.button8 -> buttonNumber = 8
                R.id.button9 -> buttonNumber = 9
            }
            if (buttonNumber != 0) {
                playGame(clickedView, buttonNumber)
            }
        }
    }

    private fun reset() {
        button1.text = ""
        button2.text = ""
        button3.text = ""
        button4.text = ""
        button5.text = ""
        button6.text = ""
        button7.text = ""
        button8.text = ""
        button9.text = ""

        button1.setBackgroundColor(Color.rgb(255,145,0))
        button2.setBackgroundColor(Color.rgb(255,145,0))
        button3.setBackgroundColor(Color.rgb(255,145,0))
        button4.setBackgroundColor(Color.rgb(255,145,0))
        button5.setBackgroundColor(Color.rgb(255,145,0))
        button6.setBackgroundColor(Color.rgb(255,145,0))
        button7.setBackgroundColor(Color.rgb(255,145,0))
        button8.setBackgroundColor(Color.rgb(255,145,0))
        button9.setBackgroundColor(Color.rgb(255,145,0))

        button1.isEnabled = true
        button2.isEnabled = true
        button3.isEnabled = true
        button4.isEnabled = true
        button5.isEnabled = true
        button6.isEnabled = true
        button7.isEnabled = true
        button8.isEnabled = true
        button9.isEnabled = true

        turn.text = "turn $name1"

        player1.text = "$name1 - $scoreX"
        player2.text = "$scoreO - $name2"

        firstPlayer.clear()
        secondtPlayer.clear()
        activePlayer = 1
    }



    private fun playGame(clickedView: Button, buttonNumber: Int) {

        if (activePlayer == 1) {

            clickedView.text = "x"
            turn.text = "turn $name2"
            clickedView.setBackgroundColor(Color.RED)
            firstPlayer.add(buttonNumber)

            activePlayer = 2
        } else {
            clickedView.text = "0"
            turn.text = "turn $name1"
            clickedView.setBackgroundColor(Color.BLUE)
            secondtPlayer.add(buttonNumber)

            activePlayer = 1
        }

        clickedView.isEnabled = false
        check()
    }

    private fun check() {
        var winnerPlayer = 0
        if (firstPlayer.contains(1) && firstPlayer.contains(2) && firstPlayer.contains(3)) {

            winnerPlayer = 1
        }
        if (secondtPlayer.contains(1) && secondtPlayer.contains(2) && secondtPlayer.contains(3)) {

            winnerPlayer = 2
        }
        if (firstPlayer.contains(4) && firstPlayer.contains(5) && firstPlayer.contains(6)) {

            winnerPlayer = 1
        }
        if (secondtPlayer.contains(4) && secondtPlayer.contains(5) && secondtPlayer.contains(6)) {

            winnerPlayer = 2
        }
        if (firstPlayer.contains(7) && firstPlayer.contains(8) && firstPlayer.contains(9)) {

            winnerPlayer = 1
        }
        if (secondtPlayer.contains(7) && secondtPlayer.contains(8) && secondtPlayer.contains(9)) {

            winnerPlayer = 2
        }
        if (firstPlayer.contains(1) && firstPlayer.contains(4) && firstPlayer.contains(7)) {

            winnerPlayer = 1
        }
        if (secondtPlayer.contains(1) && secondtPlayer.contains(4) && secondtPlayer.contains(7)) {

            winnerPlayer = 2
        }
        if (firstPlayer.contains(2) && firstPlayer.contains(5) && firstPlayer.contains(8)) {

            winnerPlayer = 1
        }
        if (secondtPlayer.contains(2) && secondtPlayer.contains(5) && secondtPlayer.contains(8)) {

            winnerPlayer = 2
        }
        if (firstPlayer.contains(3) && firstPlayer.contains(6) && firstPlayer.contains(9)) {

            winnerPlayer = 1
        }
        if (secondtPlayer.contains(3) && secondtPlayer.contains(6) && secondtPlayer.contains(9)) {

            winnerPlayer = 2
        }
        if (firstPlayer.contains(7) && firstPlayer.contains(5) && firstPlayer.contains(3)) {

            winnerPlayer = 1
        }
        if (secondtPlayer.contains(7) && secondtPlayer.contains(5) && secondtPlayer.contains(3)) {

            winnerPlayer = 2
        }
        if (firstPlayer.contains(1) && firstPlayer.contains(5) && firstPlayer.contains(9)) {

            winnerPlayer = 1
        }
        if (secondtPlayer.contains(1) && secondtPlayer.contains(5) && secondtPlayer.contains(9)) {

            winnerPlayer = 2
        }
        if (winnerPlayer == 1) {
            Toast.makeText(this, "$name1 Won!", Toast.LENGTH_SHORT).show()
            reset()
            scoreX++
            player1.text = "$name1 - $scoreX"
        }else if (winnerPlayer == 2) {
            Toast.makeText(this, "$name2 Won!", Toast.LENGTH_SHORT).show()
            reset()
            scoreO++
            player2.text = "$scoreO - $name2"
        }else if (firstPlayer.size + secondtPlayer.size == 9) {
            Toast.makeText(this, "Draw", Toast.LENGTH_SHORT).show()
            reset()
        }

    }
}
package com.example.tictactoe

import android.content.Intent
import android.media.AsyncPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {



    private lateinit var button: Button
    private lateinit var firtsPlayer: EditText
    private lateinit var secondPlayer: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        firtsPlayer = findViewById(R.id.first)
        secondPlayer = findViewById(R.id.second)

        button = findViewById(R.id.button)
        button.setOnClickListener{

            var name1=firtsPlayer.text.toString()
            var name2=secondPlayer.text.toString()

            val intent = Intent (this, startLayer::class.java)
            intent.putExtra("name1", name1)
            intent.putExtra("name2", name2)
            startActivity(intent)
            finish()

        }



    }
}